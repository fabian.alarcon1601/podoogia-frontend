import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { execPath } from 'process';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full', 
        redirectTo: '/inicio',
    },
    {
        path: 'charts',
        loadChildren: () =>
            import('modules/charts/charts-routing.module').then(m => m.ChartsRoutingModule),
    },
    {
        path: 'dashboard',
        loadChildren: () =>
            import('modules/dashboard/dashboard-routing.module').then(
                m => m.DashboardRoutingModule),
    },
    {
        path: 'inicio',
        loadChildren: () =>
            import('modules/inicio/inicio-routing.module').then(
                m => m.InicioRoutingModule),
    },
    {
        path: 'paciente',
        loadChildren: () =>
            import('modules/agenda-pacientes/agenda-pacientes-routing.module').then(
                m => m.AgendaPacientesRoutingModule),
    },
    {
        path: 'login',
        loadChildren: () =>
            import('modules/login/login-routing.module').then(
                m => m.LoginRoutingModule),
    },
    {
        path: 'horario',
        loadChildren: () =>
            import('modules/horario/horario-routing.module').then(
                m => m.HorarioRoutingModule),
    },
    {
        path: 'agregar/ficha',
        loadChildren: () =>
            import('modules/agregar-ficha-clinica/agregar-ficha-clinica-routing.module').then(
                m => m.AgregarFichaClinicaRoutingModule),
    },
    {
        path: 'agendar/hora',
        loadChildren: () =>
            import('modules/agendar-hora/agendar-hora-routing.module').then(
                m => m.AgendarHoraRoutingModule),
    },
    {
        path: 'fichas/clinicas',
        loadChildren: () =>
            import('modules/fichas-clinicas/fichas-clinicas-routing.module').then(
                m => m.FichasClinicasRoutingModule),
    },
    {
        path: 'solicitudes/psicologos',
        loadChildren: () =>
            import('modules/solicitudes-psicologos/solicitudes-psicologos-routing.module').then(
                m => m.SolicitudesPsicologosRoutingModule),
    },
    {
        path: 'registro',
        loadChildren: () =>
            import('modules/registro/registro-routing.module').then(
                m => m.RegistroRoutingModule),
    },
    {
        path: 'auth',
        loadChildren: () =>
            import('modules/auth/auth-routing.module').then(m => m.AuthRoutingModule),
    },
    {
        path: 'error',
        loadChildren: () =>
            import('modules/error/error-routing.module').then(m => m.ErrorRoutingModule),
    },
    {
        path: 'tables',
        loadChildren: () =>
            import('modules/tables/tables-routing.module').then(m => m.TablesRoutingModule),
    },
    {
        path: 'version',
        loadChildren: () =>
            import('modules/utility/utility-routing.module').then(m => m.UtilityRoutingModule),
    },
    {
        path: '**',
        pathMatch: 'full',
        loadChildren: () =>
            import('modules/error/error-routing.module').then(m => m.ErrorRoutingModule),
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
    exports: [RouterModule],
})
export class AppRoutingModule { }
