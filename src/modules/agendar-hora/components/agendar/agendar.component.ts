import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AgendarHoraService } from '@modules/agendar-hora/services';
import { ProfesionalModel } from 'models/profesional';

@Component({
    selector: 'sb-agendar',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './agendar.component.html',
    styleUrls: ['agendar.component.scss'],
})
export class AgendarComponent implements OnInit {
    profesional_id = 1;
    horarios;
    fecha;
    mostrarCreacionDePaciente:boolean;
    mostrarMensajeDeReserva:boolean;
    constructor(private router: Router, private agendarHorarioService: AgendarHoraService) {
        this.mostrarCreacionDePaciente=false;
        this.mostrarMensajeDeReserva=false;
    }
    ngOnInit() { }



    buscarDisponibilidad(fecha) {
        this.fecha = fecha;
        let data = {
            profesional_id: this.profesional_id,
            fecha
        }
        this.agendarHorarioService.buscarDisponibilidad(data).subscribe((resp: any) => {
            this.horarios = resp.data;
            console.log(this.horarios);
        })
    }

    agendar2(hora, id,fecha, rut) {
        let profesional_id = 1;
        let dataPaciente ={
            rut
        }
        
        this.agendarHorarioService.obtenerPacientePorRut(dataPaciente).subscribe((resp: any) => {
            console.log(resp);
            let paciente_id = resp.data.id
            let data = {
                fecha,
                hora,
                paciente_id,
                profesional_id,
                id
                
            }
            this.agendarHorarioService.agendar(data).subscribe((resp: any) => {
                console.log(resp);
            })
        })
    }


    agendar(hora, id) {
        let fecha = this.fecha;
        let profesional_id = 1;

        console.log(fecha, profesional_id, hora, id);

        let dataPaciente = {
            paciente_id: 1
        }

        this.agendarHorarioService.obtenerPacienteID(dataPaciente).subscribe((resp: any) => {
            console.log(resp);
            let paciente_id = resp.data.id
            let data = {
                paciente_id,
                fecha,
                profesional_id,
                hora,
                id
            }
            this.agendarHorarioService.agendar(data).subscribe((resp: any) => {
                console.log(resp);
            })
        })

    }

}
