import { analyzeAndValidateNgModules } from '@angular/compiler';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { AgendarHoraService } from '@modules/agendar-hora/services';

@Component({
    selector: 'sb-buscar-profesional',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './buscar-profesional.component.html',
    styleUrls: ['buscar-profesional.component.scss'],
})
export class BuscarProfesionalComponent implements OnInit {
    especialidades;
    profesionales;
    constructor(private agendarHoraService: AgendarHoraService, private router: Router) {}
    ngOnInit() {
        this.agendarHoraService.obtenerListadoEspecialidad().subscribe((resp:any)=>{
            this.especialidades=resp.especialidades;
        })
        this.agendarHoraService.obtenerListadoPsicologoAprobados().subscribe((resp:any)=>{
            console.log(resp);
            this.profesionales= resp.profesionales;
        })
    }
    buscar(nombre){
        let data={nombre};
        this.agendarHoraService.obtenerPsicologoPorNombre(data).subscribe((resp:any)=>{
            this.profesionales=resp.profesionales;
        })
    }

    agendar(profesional_id){
        const navigationExtras: NavigationExtras = {state: {profesional_id}};
        this.router.navigate(['/agendar/hora/agendar'],navigationExtras);
        
    }
    listarEspecialidad(nombre){
        let data={nombre};
        this.agendarHoraService.obtenerPsicologoPorEspecialidad(data).subscribe((resp:any)=>{
           this.profesionales=resp.data; 
        })

    }
}
