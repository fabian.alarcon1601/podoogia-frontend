import { TestBed } from '@angular/core/testing';

import { AgendarHoraGuard } from './agendar-hora.guard';

describe('AgendarHora Guards', () => {
    let agendarHoraGuard: AgendarHoraGuard;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [AgendarHoraGuard],
        });
        agendarHoraGuard = TestBed.inject(AgendarHoraGuard);
    });

    describe('canActivate', () => {
        it('should return an Observable<boolean>', () => {
            agendarHoraGuard.canActivate().subscribe(response => {
                expect(response).toEqual(true);
            });
        });
    });

});
