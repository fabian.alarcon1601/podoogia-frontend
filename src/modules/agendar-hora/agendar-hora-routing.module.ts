/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SBRouteData } from '@modules/navigation/models';

/* Module */
import { AgendarHoraModule } from './agendar-hora.module';

/* Containers */
import * as agendarHoraContainers from './containers';

/* Guards */
import * as agendarHoraGuards from './guards';

/* Routes */
export const ROUTES: Routes = [
    {
        path: 'buscar/profesional',
        data: {
            title: 'Buscar profesional',
        } as SBRouteData,
        canActivate: [],
        component: agendarHoraContainers.BuscarProfesionalComponent,
    },
    {
        path: 'agendar',
        data: {
            title: 'reserva de hora',
        } as SBRouteData,
        canActivate: [],
        component: agendarHoraContainers.AgendarComponent,
    },
];

@NgModule({
    imports: [AgendarHoraModule, RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
})
export class AgendarHoraRoutingModule {}
