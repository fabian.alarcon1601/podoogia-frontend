import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
    selector: 'sb-agendar-container',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './agendar.component.html',
    styleUrls: ['agendar.component.scss'],
})
export class AgendarComponent implements OnInit {
    constructor() {}
    ngOnInit() {}
}
