import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { url } from '../../../environments/environment';

@Injectable()
export class AgendarHoraService {

    private headers = new HttpHeaders(
        {  'Content-Type':  'application/json' }
      );

    constructor(private http:HttpClient) {}

    obtenerListadoPsicologoAprobados(){
        return this.http.get(url + '/profesional/listar/aprobado');
    }
    obtenerListadoEspecialidad(){
        return this.http.get(url + '/especialidad/listar');
    }
    obtenerPsicologoPorNombre(data){
        const option = {headers:this.headers};
        console.log(data);
        return this.http.post(url + '/profesional/buscar/aprobado/nombre', data, option);
    }
    obtenerPsicologoPorEspecialidad(data){
        const option = {headers:this.headers};
        return this.http.post(url + '/profesional/buscar/profesional/especialidad', data, option);
    }
    buscarDisponibilidad(data){
        const option = {headers:this.headers};
        return this.http.post(url + '/horario/buscar/disponibilidad',data, option);
    }
    agendar(data){
        const option = {headers:this.headers};
        console.log(data);
        return this.http.post(url + '/reserva/crear',data, option);
    }
    obtenerPacienteID(data){
        const option = {headers:this.headers};
        console.log(data);
        return this.http.post(url + '/paciente/buscar/usuario/id', data, option);
    }

    obtenerPacientePorRut(data){
        const option = {headers:this.headers};
        console.log(data);
        return this.http.post(url + '/paciente/buscar/rut', data, option);
    }
    obtenerPsicologoID(data){
        const option = {headers:this.headers};
        console.log(data);
        return this.http.post(url + '/paciente/buscar/usuario/id', data, option);
    }
}
