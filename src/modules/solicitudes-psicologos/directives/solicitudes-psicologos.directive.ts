import { Directive, Input } from '@angular/core';

@Directive({
    selector: '[sbSolicitudesPsicologos]',
})
export class SolicitudesPsicologosDirective {
    @Input() param!: string;

    constructor() {}
}
