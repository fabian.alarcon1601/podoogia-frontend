import { Component, DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { SolicitudesPsicologosDirective } from './solicitudes-psicologos.directive';

@Component({
    template: '<div sbSolicitudesPsicologos param="test"></div>',
})
class TestComponent {}

describe('SolicitudesPsicologosDirective', () => {
    let fixture: ComponentFixture<TestComponent>;

    let testComponent: TestComponent;
    let testComponentDE: DebugElement;
    let testComponentNE: Element;

    beforeEach(() => {
        fixture = TestBed.configureTestingModule({
            declarations: [SolicitudesPsicologosDirective, TestComponent],
        }).createComponent(TestComponent);

        fixture.detectChanges();

        testComponent = fixture.componentInstance;
        testComponentDE = fixture.debugElement;
        testComponentNE = testComponentDE.nativeElement;
    });

    it('should have param set to test', () => {
        const directiveInstance = testComponentDE.query(By.directive(SolicitudesPsicologosDirective));
        expect(directiveInstance.attributes.param).toBe('test');
    });
});
