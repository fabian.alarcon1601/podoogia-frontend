import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
    selector: 'sb-listado-solicitudes-container',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './listado-solicitudes.component.html',
    styleUrls: ['listado-solicitudes.component.scss'],
})
export class ListadoSolicitudesComponent implements OnInit {
    constructor() {}
    ngOnInit() {}
}
