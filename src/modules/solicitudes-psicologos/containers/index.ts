import { ListadoSolicitudesComponent } from './listado-solicitudes/listado-solicitudes.component';
import { SolicitudesRechazadasComponent } from './solicitudes-rechazadas/solicitudes-rechazadas.component';
import { SolicitudesAprobadasComponent } from './solicitudes-aprobadas/solicitudes-aprobadas.component';

export const containers = [ListadoSolicitudesComponent, SolicitudesRechazadasComponent, SolicitudesAprobadasComponent];

export * from './listado-solicitudes/listado-solicitudes.component';
export * from './solicitudes-rechazadas/solicitudes-rechazadas.component';
export * from './solicitudes-aprobadas/solicitudes-aprobadas.component';
