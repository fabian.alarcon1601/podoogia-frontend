/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

/* Modules */
import { AppCommonModule } from '@common/app-common.module';
import { NavigationModule } from '@modules/navigation/navigation.module';

/* Components */
import * as solicitudesPsicologosComponents from './components';

/* Containers */
import * as solicitudesPsicologosContainers from './containers';

/* Guards */
import * as solicitudesPsicologosGuards from './guards';

/* Services */
import * as solicitudesPsicologosServices from './services';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        FormsModule,
        AppCommonModule,
        NavigationModule,
    ],
    providers: [...solicitudesPsicologosServices.services, ...solicitudesPsicologosGuards.guards],
    declarations: [...solicitudesPsicologosContainers.containers, ...solicitudesPsicologosComponents.components],
    exports: [...solicitudesPsicologosContainers.containers, ...solicitudesPsicologosComponents.components],
})
export class SolicitudesPsicologosModule {}
