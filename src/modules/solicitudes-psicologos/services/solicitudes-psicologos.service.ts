import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { url } from '../../../environments/environment';

@Injectable()
export class SolicitudesPsicologosService {
    private headers = new HttpHeaders(
        {  'Content-Type':  'application/json' }
      );
    constructor(private http:HttpClient) {}

    obtenerListadoPodologoPendientes(){
        return this.http.get(url + '/profesional/listar');
    }
    obtenerListadoPodologosAprobados(){
        return this.http.get(url + '/profesional/listar/aprobado');
    }
    obtenerListadoPodologosRechazado(){
        return this.http.get(url + '/profesional/listar/rechazado');
    }
    aprobarPodologo(data){
        const option = {headers:this.headers};
        console.log(data);
        return this.http.post(url + '/profesional/actualizacion/estado',data,option);
    }

}
