import { TestBed } from '@angular/core/testing';

import { SolicitudesPsicologosService } from './solicitudes-psicologos.service';

describe('SolicitudesPsicologosService', () => {
    let solicitudesPsicologosService: SolicitudesPsicologosService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [SolicitudesPsicologosService],
        });
        solicitudesPsicologosService = TestBed.inject(SolicitudesPsicologosService);
    });

    describe('getSolicitudesPsicologos$', () => {
        it('should return Observable<SolicitudesPsicologos>', () => {
            expect(solicitudesPsicologosService).toBeDefined();
        });
    });
});
