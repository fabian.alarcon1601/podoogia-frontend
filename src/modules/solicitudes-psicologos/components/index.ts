import { ListadoSolicitudesComponent } from './listado-solicitudes/listado-solicitudes.component';
import { SolicitudesAprobadasComponent } from './solicitudes-aprobadas/solicitudes-aprobadas.component';
import { SolicitudesRechazadasComponent } from './solicitudes-rechazadas/solicitudes-rechazadas.component';

export const components = [ListadoSolicitudesComponent, SolicitudesAprobadasComponent, SolicitudesRechazadasComponent];

export * from './listado-solicitudes/listado-solicitudes.component';
export * from './solicitudes-aprobadas/solicitudes-aprobadas.component';
export * from './solicitudes-rechazadas/solicitudes-rechazadas.component';
