import { Component, OnInit } from '@angular/core';
import { SolicitudesPsicologosService } from '@modules/solicitudes-psicologos/services';

@Component({
    selector: 'sb-listado-solicitudes',
    templateUrl: './listado-solicitudes.component.html',
    styleUrls: ['listado-solicitudes.component.scss'],
})
export class ListadoSolicitudesComponent implements OnInit {
    podologos;
    constructor(private solicitudesPsicologosService: SolicitudesPsicologosService) {
    }
    ngOnInit() {
        this.listado();
    }
    aprobar(numero,id){
        console.log(numero,id);
        let dia:any = new Date().getDate();
        let mes:any = new Date().getMonth()+1;
        let anio = new Date().getFullYear();
        if(mes < 10){
            mes = '0' + mes;
        }
        if(dia < 10){
            dia = '0' + dia;
        }
        let actualizacion_solicitud = anio + '-' + mes + '-' + dia;
        let data = {
            id:id,
            estado_profesional_id:numero,
            actualizacion_solicitud
        };
        
        this.solicitudesPsicologosService.aprobarPodologo(data).subscribe((resp:any)=>{
            console.log(resp);
            this.listado();

        })
         
    }
    listado(){
        this.solicitudesPsicologosService.obtenerListadoPodologoPendientes().subscribe((resp:any)=>{
            console.log(resp.profesionales)
           this.podologos= resp.profesionales;
        })
    }
    
}
