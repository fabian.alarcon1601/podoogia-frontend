import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
    selector: 'sb-agregar-ficha-container',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './agregar-ficha.component.html',
    styleUrls: ['agregar-ficha.component.scss'],
})
export class AgregarFichaComponent implements OnInit {
    constructor() {}
    ngOnInit() {}
}
