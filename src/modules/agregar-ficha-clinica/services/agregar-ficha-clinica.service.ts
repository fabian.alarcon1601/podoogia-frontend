import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { url } from '../../../environments/environment';
import { Observable, of } from 'rxjs';

@Injectable()
export class AgregarFichaClinicaService {

    private headers = new HttpHeaders(
        {  'Content-Type':  'application/json' }
    );

    constructor(private http:HttpClient) {}

    obtenerPaciente(data){
        const option = {headers:this.headers};
        console.log(data);
        return this.http.post(url + '/paciente/buscar', data, option);
    }

    buscarFicha(data){
        const option = {headers:this.headers};
        console.log(data);
        return this.http.post(url + '/fichas/buscar/paciente', data, option);
    }

    guardarFichaClinica(data){
        const option = {headers:this.headers};
        console.log(data);
        return this.http.post(url + '/fichas/crear', data, option);
    }
    editarFichaClinica(data){
        const option = {headers:this.headers};
        console.log(data);
        return this.http.post(url + '/fichas/editar', data, option);
    }
}
