import { AgregarFichaClinicaGuard } from './agregar-ficha-clinica.guard';

export const guards = [AgregarFichaClinicaGuard];

export * from './agregar-ficha-clinica.guard';
