/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SBRouteData } from '@modules/navigation/models';

/* Module */
import { AgregarFichaClinicaModule } from './agregar-ficha-clinica.module';

/* Containers */
import * as agregarFichaClinicaContainers from './containers';

/* Guards */
import * as agregarFichaClinicaGuards from './guards';

/* Routes */
export const ROUTES: Routes = [
    {
        path: 'clinica',
        data: {
            title: 'Agregar ficha clínica',
        } as SBRouteData,
        canActivate: [],
        component: agregarFichaClinicaContainers.AgregarFichaComponent,
    },
];

@NgModule({
    imports: [AgregarFichaClinicaModule, RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
})
export class AgregarFichaClinicaRoutingModule {}
