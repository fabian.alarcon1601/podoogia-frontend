/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

/* Modules */
import { AppCommonModule } from '@common/app-common.module';
import { NavigationModule } from '@modules/navigation/navigation.module';

/* Components */
import * as agregarFichaClinicaComponents from './components';

/* Containers */
import * as agregarFichaClinicaContainers from './containers';

/* Guards */
import * as agregarFichaClinicaGuards from './guards';

/* Services */
import * as agregarFichaClinicaServices from './services';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        FormsModule,
        AppCommonModule,
        NavigationModule,
    ],
    providers: [...agregarFichaClinicaServices.services, ...agregarFichaClinicaGuards.guards],
    declarations: [...agregarFichaClinicaContainers.containers, ...agregarFichaClinicaComponents.components],
    exports: [...agregarFichaClinicaContainers.containers, ...agregarFichaClinicaComponents.components],
})
export class AgregarFichaClinicaModule {}
