import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AgregarFichaClinicaService } from '@modules/agregar-ficha-clinica/services';

@Component({
    selector: 'sb-agregar-ficha',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './agregar-ficha.component.html',
    styleUrls: ['agregar-ficha.component.scss'],
})
export class AgregarFichaComponent implements OnInit {
    paciente_id;
    paciente;
    ficha;
    editar;
    motivo_consulta;

    constructor(private router: Router,
        private agregarFichaService: AgregarFichaClinicaService) {
        const navigation = this.router.getCurrentNavigation();
        this.paciente_id = navigation.extras.state.paciente_id;
        this.editar = navigation.extras.state.editar;
        let data = {
            id: this.paciente_id,
        }
        console.log(this.editar);
        if (this.editar === 0) {
            this.agregarFichaService.obtenerPaciente(data).subscribe((resp: any) => {
                console.log(resp);
                this.paciente = resp.data
            });
        } else {
            this.agregarFichaService.buscarFicha(data).subscribe((resp: any) => {
                console.log(resp)
                this.ficha = resp.data;
                this.paciente = resp.data.paciente;
            })
        }


    }
    ngOnInit() {
    }
    atras() {
        this.router.navigate(['/fichas/clinicas/ficha/clinica']);
    }
    guardar(nombres, apellido_paterno, apellido_materno, correo, telefono, rut, n_emergencia) {
        let dia: any = new Date().getDate();
        let mes: any = new Date().getMonth() + 1;
        let anio = new Date().getFullYear();
        if (mes < 10) {
            mes = '0' + mes;
        }
        if (dia < 10) {
            dia = '0' + dia;
        }
        let fecha = anio + '-' + mes + '-' + dia;

        if (this.editar === 0) {
            let data = {
                paciente_id: this.paciente_id,
                nombres,
                apellido_paterno,
                apellido_materno,
                correo, telefono,
                rut,
                n_emergencia
            }
            console.log(data);
            this.agregarFichaService.guardarFichaClinica(data).subscribe((resp: any) => {
                if (resp.code === 200) {
                    
                    this.router.navigate(['/fichas/clinicas/ficha/clinica']);
                }
            })
        } else {
            if (this.editar === 1) {
                let data = {
                    id: this.ficha.id,
                    paciente_id: this.paciente_id,
                    nombres,
                    apellido_paterno,
                    apellido_materno,
                    correo, telefono,
                    rut,
                    n_emergencia
                }
                console.log(data);
                this.agregarFichaService.editarFichaClinica(data).subscribe((resp: any) => {
                    if (resp.code === 200) {
                        this.router.navigate(['/fichas/clinicas/ficha/clinica']);
                    }
                })
            }
        }
    }
}
