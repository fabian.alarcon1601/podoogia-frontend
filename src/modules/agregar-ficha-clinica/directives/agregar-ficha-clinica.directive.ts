import { Directive, Input } from '@angular/core';

@Directive({
    selector: '[sbAgregarFichaClinica]',
})
export class AgregarFichaClinicaDirective {
    @Input() param!: string;

    constructor() {}
}
