/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/* Module */
import { RegistroModule } from './registro.module';

/* Containers */
import * as registroContainers from './containers';

/* Guards */
import * as registroGuards from './guards';
import { SBRouteData } from '@modules/navigation/models';

/* Routes */
export const ROUTES: Routes = [
    {
        path: 'general',
        data: {
            title: 'Registro',
        } as SBRouteData,
        canActivate: [],
        component: registroContainers.RegistroGeneralComponent,
    },

];

@NgModule({
    imports: [RegistroModule, RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
})
export class RegistroRoutingModule {}
