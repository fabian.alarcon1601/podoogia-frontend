import { TestBed } from '@angular/core/testing';

import { RegistroGuard } from './registro.guard';

describe('Registro Guards', () => {
    let registroGuard: RegistroGuard;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [RegistroGuard],
        });
        registroGuard = TestBed.inject(RegistroGuard);
    });

    describe('canActivate', () => {
        it('should return an Observable<boolean>', () => {
            registroGuard.canActivate().subscribe(response => {
                expect(response).toEqual(true);
            });
        });
    });

});
