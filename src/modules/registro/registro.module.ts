/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

/* Modules */
import { AppCommonModule } from '@common/app-common.module';
import { NavigationModule } from '@modules/navigation/navigation.module';

/* Components */
import * as registroComponents from './components';

/* Containers */
import * as registroContainers from './containers';

/* Guards */
import * as registroGuards from './guards';

/* Services */
import * as registroServices from './services';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        FormsModule,
        AppCommonModule,
        NavigationModule,
    ],
    providers: [...registroServices.services, ...registroGuards.guards],
    declarations: [...registroContainers.containers, ...registroComponents.components],
    exports: [...registroContainers.containers, ...registroComponents.components],
})
export class RegistroModule {}
