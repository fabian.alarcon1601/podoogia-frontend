import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { url } from '../../../environments/environment';

@Injectable()
export class RegistroService {
    private headers = new HttpHeaders(
        {  'Content-Type':  'application/json' }
      );
    constructor(private http: HttpClient) {}

    obtenerListadoEspecialidad(){
        return this.http.get(url + '/especialidad/listar');
    }
    registroUsuario(data){
        const option = {headers:this.headers};
        return this.http.post(url + '/usuario/crear',data, option);
    }
    
    registroProfesional(data){
        const option = {headers:this.headers};
        return this.http.post(url + '/profesional/crear',data,option);
    }       
}
