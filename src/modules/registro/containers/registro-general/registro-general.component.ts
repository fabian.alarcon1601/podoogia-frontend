import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'sb-registro-general',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './registro-general.component.html',
    styleUrls: ['registro-general.component.scss'],
})
export class RegistroGeneralComponent implements OnInit {
    psicologo:number=0;

    constructor(private router:Router) {}
    ngOnInit() {}

    escogerPsicologo() {
        this.psicologo=1;  
    }


    inicio(){
        this.router.navigate(['/inicio']);
    }
    login(){
        this.router.navigate(['/login']);
    }
}
