import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { RegistroService } from '@modules/registro/services';
import {NgForm} from '@angular/forms';
import {ProfesionalModel} from '../../../../models/profesional';
import {UsuarioModel} from '../../../../models/usuario';

@Component({
    selector: 'sb-registro-psicologo',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './registro-psicologo.component.html',
    styleUrls: ['registro-psicologo.component.scss'],
})
export class RegistroPsicologoComponent implements OnInit {
    especialidades: [];
    profesional:ProfesionalModel;
    usuario:UsuarioModel;
    success:boolean;
    error:boolean;
    message:string;
    constructor(private registroService: RegistroService) {
        this.profesional = new ProfesionalModel();
        this.usuario = new UsuarioModel();
    }
    ngOnInit() {
        this.registroService.obtenerListadoEspecialidad().subscribe((resp: any) => {
            this.especialidades = resp.especialidades;
        })
    }
    registrar(form:NgForm, rePass){
        if(rePass==this.usuario.pass){
            this.usuario.rol_id = 2;
            this.registroService.registroUsuario(this.usuario).subscribe((resp:any)=>{
                console.log(resp);
                if(resp.code==400){
                    this.error=true;
                    this.message='Ups! hubo un error al crear al paciente';
                }
                this.profesional.usuario_id=resp.data.id;
                this.profesional.activo = true;
                this.registroService.registroProfesional(this.profesional).subscribe((resp:any)=>{
                    if(resp.code==400){
                        this.error=true;
                        this.message='Ups! hubo un error al crear al paciente';
                    }
                    if(resp.code == 200){
                        this.success=true;
                        this.message='Se creó el profesional exitosamente';
                    }
                })

            });
        }else{
            this.error=true;
            this.message='las contraseñas no son iguales';
        }
    }
}
