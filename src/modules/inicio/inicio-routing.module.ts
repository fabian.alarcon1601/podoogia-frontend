/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/* Module */
import { InicioModule } from './inicio.module';

/* Containers */
import * as inicioContainers from './containers';


/* Guards */
import * as inicioGuards from './guards';
import { SBRouteData } from '@modules/navigation/models';
import { EditarInicioComponent } from './containers';
import { EditarFooterComponent } from './containers';

/* Routes */
export const ROUTES: Routes = [
    {
        path: '',
        data: {
            title: 'inicio',
        } as SBRouteData,
        canActivate: [],
        component: inicioContainers.PaginaPrincipalComponent,
    },
    {
        path: 'editar/inicio',
        data: {
            title: 'Editar Inicio',
        } as SBRouteData,
        canActivate: [],
        component: inicioContainers.EditarInicioComponent
    },
    {
        path: 'editar/footer',
        data: {
            title: 'Editar footer',
        } as SBRouteData,
        canActivate: [],
        component: inicioContainers.EditarFooterComponent 
    },
];


@NgModule({
    imports: [InicioModule, RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
})
export class InicioRoutingModule {}
