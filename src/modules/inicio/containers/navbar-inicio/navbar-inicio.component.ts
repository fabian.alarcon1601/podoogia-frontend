import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
    selector: 'sb-navbar-inicio-component',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './navbar-inicio.component.html',
    styleUrls: ['navbar-inicio.component.scss'],
})
export class NavbarInicioComponent implements OnInit {
    constructor() {}
    ngOnInit() {}
}
