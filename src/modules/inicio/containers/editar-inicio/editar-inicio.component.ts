import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
    selector: 'sb-editar-inicio-container',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './editar-inicio.component.html',
    styleUrls: ['editar-inicio.component.scss'],
})
export class EditarInicioComponent implements OnInit {
    constructor() {}
    ngOnInit() {}
}
