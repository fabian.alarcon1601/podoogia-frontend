import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
    selector: 'sb-editar-footer-container',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './editar-footer.component.html',
    styleUrls: ['editar-footer.component.scss'],
})
export class EditarFooterComponent implements OnInit {
    constructor() {}
    ngOnInit() {}
}
