import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
    selector: 'sb-pagina-principal-container',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './pagina-principal.component.html',
    styleUrls: ['pagina-principal.component.scss'],
})
export class PaginaPrincipalComponent implements OnInit {
    constructor() {}
    ngOnInit() {}
}
