import { PaginaPrincipalComponent } from './pagina-principal/pagina-principal.component';
import { FooterComponent } from './footer/footer.component';
import { EditarInicioComponent } from './editar-inicio/editar-inicio.component';
import { NavbarInicioComponent } from './navbar-inicio/navbar-inicio.component';
import { EditarFooterComponent } from './editar-footer/editar-footer.component';

export const containers = [PaginaPrincipalComponent, FooterComponent, EditarInicioComponent, NavbarInicioComponent, EditarFooterComponent];

export * from './pagina-principal/pagina-principal.component';
export * from './footer/footer.component';
export * from './editar-inicio/editar-inicio.component';
export * from './navbar-inicio/navbar-inicio.component';
export * from './editar-footer/editar-footer.component';
