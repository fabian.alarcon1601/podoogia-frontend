/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

/* Modules */
import { AppCommonModule } from '@common/app-common.module';
import { NavigationModule } from '@modules/navigation/navigation.module';

/* Components */
import * as inicioComponents from './components';

/* Containers */
import * as inicioContainers from './containers';

/* Guards */
import * as inicioGuards from './guards';

/* Services */
import * as inicioServices from './services';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        FormsModule,
        AppCommonModule,
        NavigationModule,
    ],
    providers: [...inicioServices.services, ...inicioGuards.guards],
    declarations: [...inicioContainers.containers, ...inicioComponents.components],
    exports: [...inicioContainers.containers, ...inicioComponents.components],
})
export class InicioModule {}
