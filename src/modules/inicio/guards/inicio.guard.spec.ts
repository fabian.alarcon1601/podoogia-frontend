import { TestBed } from '@angular/core/testing';

import { InicioGuard } from './inicio.guard';

describe('Inicio Guards', () => {
    let inicioGuard: InicioGuard;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [InicioGuard],
        });
        inicioGuard = TestBed.inject(InicioGuard);
    });

    describe('canActivate', () => {
        it('should return an Observable<boolean>', () => {
            inicioGuard.canActivate().subscribe(response => {
                expect(response).toEqual(true);
            });
        });
    });

});
