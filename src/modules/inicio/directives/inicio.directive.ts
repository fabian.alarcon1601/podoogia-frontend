import { Directive, HostBinding, HostListener, Input } from '@angular/core';

@Directive({
    selector: '[sbInicio]',
})
export class InicioDirective {
    @Input() param!: string;

    constructor() {}

    @HostBinding('class.new-nav')newNav:boolean;

    @HostListener('window:scroll')onScroll(){
        console.log(window.scrollY);
        if (window.scrollY >= 50){
            this.newNav = true;
        }else{
            this.newNav = false;
        }
    }
}
