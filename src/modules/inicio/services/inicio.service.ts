import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { url } from 'environments/environment';
import { Observable, of } from 'rxjs';

@Injectable()
export class InicioService {
    private headers = new HttpHeaders(
        {  'Content-Type':  'application/json' }
    );
    constructor(private http:HttpClient) {}

    

    obtenerListadoInicio(){
        return this.http.get(url + '/inicio/listar');
    }

    obtenerListadoFooter(){
        return this.http.get(url + '/footer/listar');
    }

    buscarDisponibilidad(data){
        const option = {headers:this.headers};
        console.log(data);
        return this.http.post(url + '/horario/buscar/disponibilidad' , data , option);
    }

    crearReservacion(data){
        
    }
    
    editarPaginaInicio(data){
        const option = {headers:this.headers};
        return this.http.post(url + '/inicio/editar', data, option);
    }

    editarPaginaInicioFooter(data){
        const option = {headers:this.headers};
        return this.http.post(url + '/footer/editar', data, option);
    }
}
