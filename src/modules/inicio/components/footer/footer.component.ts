import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
    selector: 'sb-footer',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './footer.component.html',
    styleUrls: ['footer.component.scss'],
})
export class FooterComponent implements OnInit {
    constructor() {}
    ngOnInit() {}

    toInicio(){
        document.getElementById("inicio").scrollIntoView({behavior:"smooth"});
    }

    toAgendarHora(){
        document.getElementById("disponibilidad-horaria").scrollIntoView({behavior:"smooth"});
    }

    toServicios(){
        document.getElementById("servicios").scrollIntoView({behavior:"smooth"});
    }

    toMisionVision(){
        document.getElementById("misionVision").scrollIntoView({behavior:"smooth"});
    }

    toAboutServices(){
        document.getElementById("about-services").scrollIntoView({behavior:"smooth"});
    }

    toContacto(){
        document.getElementById("contacto").scrollIntoView({behavior:"smooth"});
    }
}
