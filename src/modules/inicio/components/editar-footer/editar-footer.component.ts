import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { InicioService } from '@modules/inicio/services';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'sb-editar-footer',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './editar-footer.component.html',
    styleUrls: ['editar-footer.component.scss'],
})
export class EditarFooterComponent implements OnInit {
    footer:any;

    constructor(private inicioService: InicioService, private modal: NgbModal) {

    }
    
    ngOnInit() {
        this.obtenerListado();
    }


    obtenerListado(){
        this.inicioService.obtenerListadoFooter().subscribe((resp: any) => {
            console.log(resp);
            this.footer = resp.Footer;
        })

    }

    openModal(contenido) {
        this.modal.open(contenido,{size:'xl',centered:true});
    }

    editar(numero_telefono, numero_celular, instagram , facebook , whatsapp) {
        let data = {
            id:this.footer.id,
            numero_telefono,numero_celular,instagram,facebook,whatsapp
        }
        console.log(data);
        this.inicioService.editarPaginaInicioFooter(data).subscribe((resp:any)=>{
            console.log(resp);
            if(resp.code===200){
                console.log("entro");
                this.footer = resp.data;
                console.log(this.footer);
                this.modal.dismissAll();

            }
        })
    }
}

