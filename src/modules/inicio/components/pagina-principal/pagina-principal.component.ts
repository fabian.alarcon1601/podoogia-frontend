import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { InicioService } from '@modules/inicio/services';
import Swal from 'sweetalert2';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'sb-pagina-principal',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './pagina-principal.component.html',
    styleUrls: ['pagina-principal.component.scss'],
})
export class PaginaPrincipalComponent implements OnInit {
  inicio:any; 
  buscarFecha:any; 
  
  constructor( private router:Router, private inicioService:InicioService, private modal:NgbModal) {}
  controlVistas: string = 'imagen';
   
   
   ngOnInit() {
     this.inicioService.obtenerListadoInicio().subscribe((resp:any)=>{
      console.log(resp);
      this.inicio=resp.Inicio;
     })
   }
   
   buscar(fecha,profesional_id){
      let data = {fecha,profesional_id};
      this.inicioService.buscarDisponibilidad(data).subscribe((resp:any)=>{
        console.log(resp);
        this.buscarFecha= resp.data;
      })
   }

  
    
    registro(){
        this.router.navigate(['/registro/general']);
    }

    showModal1() {
        Swal.fire({
          title:
            '<strong><u>¿Que es la onicocriptosis (Uña encarnada)?</u></strong>',
          html:
          '<div align="justify">Es una Incrustación de la espícula de la lámina ungueal causada por alteraciones anatómicas o mecánicas que permiten la penetración de esta lámina en los bordes laterales de las partes blandas del dedo del pie, pudiendo ser unilateral o bilateral. </div>'+
          '<div><center><br><b>¿Cuáles son las causas?</b></center><br></div>'+
          '<div >'+
            '<p align="left"><u> La onicocriptosis aparece por los siguientes factores mecánicos:</u></p>'+
            '<p align="left">• <b>Presión directa en la uña:</b> la uña se clava en los dos bordes laterales, provocando una lesión interna y externa.</p>'+
            '<p align="left">• <b>Presión indirecta en la uña:</b> resultado de la suma de dos presiones. <br>Como por ejemplo, una presión desde fuera que aprieta al dedo gordo contra el segundo. Éste recibe una presión que lo acuña sobre el borde externo del dedo gordo y se forma así un tope en el que se clava la uña, produciendo una lesión externa.</p>'+
      
            '<p align="left"><u>Además de estas causas mecánicas, pueden darse otros factores como:</u></p>'+
            '<p align="left">• <b>Calzado:</b> El dedo gordo choca contra la extremidad de un zapato demasiado corto o puntiagudo.</p>'+
            '<p align="left">• <b>Uña</b>: El mal cortado de la uña, realizado oblicuamente en la unión del borde libre y de un borde lateral,facilita la encarnación.</p>'+
            '<p align="left">• <b>Dedo</b>: Un dedo gordo demasiado largo o desviado ofrece su falange a la compresión del calzado.</p>'+
            '<p align="left">• <b>Tejidos</b>: En algunos casos se observa un trastorno del crecimiento, con desarmonía entre el desarrollo de la uña y de las partes blandas, cuya exuberancia provoca la lesión.</p>'+
            '<p align="left">• <b>Hiperhidrosis y abuso de baños calientes:</b> Fragilizan la prominencia periungueal.</p>'+
            '<p align="left">• <b>Traumatismos:</b> Puede actuar indirectamente, lesionando la matriz, y directamente, hundiendo una arista ungueal en los tejidos vecinos.</p>'+
          '</div>',
          padding: '0',
          width: '70%',
          grow: 'fullscreen',
          showCloseButton: true,
          showCancelButton: false,
          focusConfirm: false,
          confirmButtonText: '<i class="fa fa-thumbs-up"></i> cerrar',
          confirmButtonAriaLabel: 'Thumbs up, great!',
        });
      }
      showModal2() {
        Swal.fire({
          title:
            '<strong><u>¿Que es la onicogrifosis (Uña engrosada)?</u></strong>',
          html:
          '<div align="justify">La onicogrifosis es un aumento exagerado del grosor de la lámina ungueal dándole un aspecto de cuerno o garfio con diferentes formas o direcciones. Es una patología común en personas mayores debido a traumatismos vasculares periféricas o artrosis, aunque también puede presentarse en personas que practican deporte o bien afecta de psoriasis o esclerodermia. </div>'+
          '<div align="justify">En el caso de la psoriasis y la esclerodermia, el aumento del grosor ungueal es más moderado y afecta a manos y pies. La población deportista suele sufrir de hematomas subungueales los cuales pueden alterar la matriz provocando deformidades ungueales. </div>'+
          '<div><center><br><b>¿Cuáles son las causas?</b></center><br></div>'+
    
          '<div align="justify">Los motivos de la onicogrifosis son como consecuencia de agentes internos y externos, si bien la onicogrifosis aparece de forma tardía a la causa. Los traumatismos sobre la matriz ungueal como por ejemplo calzados inadecuados, pisotones, pérdidas de uñas, caídas de objetos pesados, así como insuficiencia del riego sanguíneo en la matriz pueden producir lesiones irreversibles. </div>'+
          '<div align="justify"><br>Los pacientes afectos de onicogrifosis sufren problemas con algunos calzados debidos al grosor de la uña. Esta puede impedir calzarse y puede provocar dolor por compresión. </div>'+
          '<div align="justify"><br>La compresión que ejerce el zapato sobre la uña distrófica puede comportar la aparición de callosidades subungueales.</div>',
          padding: '0',
          width: '70%',
          grow: 'fullscreen',
          showCloseButton: true,
          showCancelButton: false,
          focusConfirm: false,
          confirmButtonText: '<i class="fa fa-thumbs-up"></i> cerrar',
          confirmButtonAriaLabel: 'Thumbs up, great!',
        });
      }
      showModal3() {
        Swal.fire({
          title:
            '<strong><u>¿Que es una verruga plantar?</u></strong>',
          html:
          '<div align="justify">Las verrugas en los pies son una infección vírica causada por el virus del papiloma humano o VPH. Se aloja en las dos primeras capas de la piel (epidermis y dermis), pero nunca llega a capas más profundas.</div>'+
          '<div align="justify">Su apariencia es parecida a una coliflor (existiendo diferentes tamaños, debido a la extensión de la lesión y la antigüedad de la misma) en la cual aparecen normalmente puntos negros (vascularización del virus) que al deslaminar produce un sangrado. </div>'+
          '<div align="justify">Suele producir dolor al pellizcar en vez de a la presión. No obstante, cuando la verruga plantar está recubierta por una capa de queratina, puede molestar al presionar la zona.</div>'+
          '<div><center><br><b>¿Cuáles son las causas?</b></center><br></div>'+
          '<div >'+
            '<div align="justify">Las verrugas plantares son causadas por el contacto directo con el virus del papiloma humano (VPH). Este es el mismo virus que causa las verrugas en otras áreas del cuerpo. Las verrugas en los pies se producen por contagio directo cuando el pie, a través de heridas o grietas, entra en contacto con el virus. </div>'+
            '<p align="left"><br><u> Los síntomas de una verruga plantar pueden incluir:</u></p>'+
    
            '<p align="left">• <b>Piel mas gruesa:</b> A menudo una verruga plantar se asemeja a una callosidad debido a su tejido duro y grueso.</p>'+
            '<p align="left">• <b>Dolor:</b> Caminar y estar de pie puede ser doloroso. Presionar los lados de la verruga también puede provocar dolor.</p>'+
            '<p align="left">• <b>Pequeños puntos negros:</b> A menudo estos aparecen en la superficie de la verruga. Los puntos son realmente sangre seca contenida en los vasos capilares (pequeños vasos sanguíneos).</p>'+
      
    
          '<div align="justify">Las verrugas plantares crecen muy profundo en la piel. Este crecimiento generalmente ocurre en forma lenta; la verruga empieza de tamaño pequeño y crece a lo largo del tiempo.</div>'+
    
          '</div>',
          padding: '0',
          width: '70%',
          grow: 'fullscreen',
          showCloseButton: true,
          showCancelButton: false,
          focusConfirm: false,
          confirmButtonText: '<i class="fa fa-thumbs-up"></i> cerrar',
          confirmButtonAriaLabel: 'Thumbs up, great!',
        });
      }
      showModal4() {
        Swal.fire({
          title:
            '<strong><u>¿Que es la onicofosis (Durezas en surcos)?</u></strong>',
          html:
          '<div align="justify">La onicofosis es una afección realmente molesta y en algunos casos, ¡muy dolorosa! es la formación de hiperqueratosis (durezas) en los surcos ungueales, y muchas veces es la causante de la onicocriptosis (uña encarnada)</div>'+
          '<div><center><br><b>¿Cuáles son las causas?</b></center><br></div>'+
          '<div >'+
          '<div align="justify">Detectables como callosidades que se forman en el surco ungueal, para compensar generalmente la falta de uña por cortes exagerados, generados por la hiper-curvatura de la uña involuta o compresiva que genera presión en los laterales del lecho provocando mucho dolor.</div>'+
          '</div>',
          padding: '0',
          width: '70%',
          grow: 'fullscreen',
          showCloseButton: true,
          showCancelButton: false,
          focusConfirm: false,
          confirmButtonText: '<i class="fa fa-thumbs-up"></i> cerrar',
          confirmButtonAriaLabel: 'Thumbs up, great!',
        });
      }
      showModal5() {
        Swal.fire({
          title:
            '<strong><u>¿Que es una lamina involuta?</u></strong>',
          html:
          '<div align="justify">Las láminas involutas o en pinzas es definida por una sobre curvatura de uno o ambos bordes laterales hacia el lecho ungueal, esta curvatura excesiva se aprecia más comúnmente en el primer ortejo, pero no excluye al resto, donde puede producir inflamación y dolor crónicos a medida que comprime los tejidos subungueales y periungueales.</div>'+
          '<div><center><br><b>¿Cuáles son las causas?</b></center><br></div>'+
          '<div >'+
          '<div align="justify">Habitualmente son adquiridas, se deben a calzados que ajustan mal, práctica de deportes con zapatillas inadecuadas, onicotomía agresiva, onicofagia en algunos casos.</div>'+
    
            '<p align="left"><br><u>Factores predisponentes:</u></p>'+
            '<p align="left">• <b>Obesidad.</b></p>'+
            '<p align="left">• <b>Hiperhidrosis.</b></p>'+
            '<p align="left">• <b>Onicotomia incorrecta.</b></p>'+
            '<p align="left">• <b>Calzado inadecuado.</b></p>'+
            '<p align="left">• <b>onicomicosis.</b></p>'+
          '</div>',
          padding: '0',
          width: '70%',
          grow: 'fullscreen',
          showCloseButton: true,
          showCancelButton: false,
          focusConfirm: false,
          confirmButtonText: '<i class="fa fa-thumbs-up"></i> cerrar',
          confirmButtonAriaLabel: 'Thumbs up, great!',
        });
      }
      showModal6() {
        Swal.fire({
          title:
            '<strong><u>¿Que es un heloma (Ojo de gallo, callo)?</u></strong>',
          html:
          '<div align="justify">Este tipo de callosidades se manifiestan principalmente en los dedos, en las áreas metatarsiana y en la cara dorsal de las articulaciones interfalángicas. </div>'+
          '<div align="justify">En los dedos en garra se puede encontrar este tipo de heloma, debido a que la parte superior de las falanges roza contra el interior del calzado.</div>'+
          '<div align="justify">Un zapato inadecuado puede provocar zonas de roce por una costura interior mal diseñada, una horma muy estrecha, un numero inapropiado o un material nuevo o duro. </div>'+
          '<div><center><br><b>¿Cuáles son las causas?</b></center><br></div>'+
          '<div >'+
            '<p align="left"><u> Algunas de las causas más frecuentes de la aparición de callos en los pies son:</u></p>'+
            '<p align="left">• <b>Un zapato inadecuado a la anatomía del pie.</b></p>'+
            '<p align="left">• <b>Deformidades en los pies (artrosis, dedos en garra, dedos en martillo, hallux valgus).</b></p>'+
            '<p align="left">• <b>Deshidratación de la piel.</b></p>'+
            '<p align="left">• <b>Apoyo incorrecto del pie contar la superficie.</b></p>'+
            '<p align="left">• <b>Alteraciones estructurales y biomecánicas de los pies. </b></p>'+
          '</div>',
          padding: '0',
          width: '70%',
          grow: 'fullscreen',
          showCloseButton: true,
          showCancelButton: false,
          focusConfirm: false,
          confirmButtonText: '<i class="fa fa-thumbs-up"></i> cerrar',
          confirmButtonAriaLabel: 'Thumbs up, great!',
        });
      }
      showModal7() {
        Swal.fire({
          title:
            '<strong><u>¿Que es la Queratosis (Callosidades, hiperqueratosis)?</u></strong>',
          html:
          '<div align="justify">Los callos o durezas en los pies son la consecuencia de un microtraumatismo repetido.</div>'+
          '<div align="justify">A cada paso que damos, el pie se dobla y se alarga en el zapato provocando roces, presión o sobrecarga de un área específica del pie. Estos microtraumatismos ocasionan que la piel vaya perdiendo elasticidad y tornándose dura.</div>'+
          '<div><center><br><b>¿Cuáles son las causas?</b></center><br></div>'+
          '<div >'+
          '<div align="justify">Estas callosidades dependen de microtraumatismos, en este caso la piel recibe un exceso de presión.</div>'+
          '<div align="justify"><br>Habitualmente podemos encontrarlos en la zona plantar, cabezas de metatarsianos y el talón.</div>'+
          '<div align="justify"><br>Surgen debido a que el tejido que sirve como amortiguador del pie, en su mayor parte adiposo, es aplastado entre el hueso y el suelo; lo que hace que la capa de grasa disminuya y favorece la aparición de dolores.</div>'+
    
          '</div>',
          padding: '0',
          width: '70%',
          grow: 'fullscreen',
          showCloseButton: true,
          showCancelButton: false,
          focusConfirm: false,
          confirmButtonText: '<i class="fa fa-thumbs-up"></i> cerrar',
          confirmButtonAriaLabel: 'Thumbs up, great!',
        });
      }
      showModal8() {
        Swal.fire({
          title:
            '<strong><u>¿Que es la onicomicosis (Hongo en uñas)</u></strong>',
          html:
          '<div align="justify">La onicomicosis es una enfermedad infecciosa de las uñas causada por distintos tipos de hongos. Puede afectar indistintamente a las uñas de las manos y los pies, aunque lo más frecuente es que se produzcan en los pies, especialmente en los dedos gordos. </div>'+
          '<div align="justify">Los primeros síntomas de la onicomicosis se manifiestan con la aparición de manchas blancas en el borde exterior o el más próximo a la cutícula, que en la medida en que evoluciona se van extendiendo al resto de su superficie. Además, la infección puede causar el ablandamiento de la uña y, como consecuencia, la deformación de la misma. Al ser una enfermedad infecciosa, es también contagiosa, pudiéndose extender de una uña a otra e incluso a otras personas. </div>'+
          '<div><center><br><b>¿Cuáles son las causas?</b></center><br></div>'+
          '<div >'+
          '<div align="justify">El medio de contagio idóneo para los hongos de las uñas es la humedad, que se incrementa por un exceso de sudoración o hiperhidrosis o un mal secado de pies, entre otros. Esta patología también puede contraerse tras frecuentar zonas deportivas como piscinas, vestuarios o duchas públicas. Por ello, es fundamental utilizar chanclas en estos espacios para evitarlo en la medida de lo posible. </div>'+
          '<div align="justify"><br>La aparición de hongos no siempre tiene su origen en un contagio, es posible que el hongo se encuentre en la piel anteriormente, pero las condiciones de calor y temperatura, sobre todo en verano, generen su proliferación. </div>'+
      
            '<p align="left"><u><br>Los factores de riesgo para contraer la onicomicosis incluyen:</u></p>'+
            '<p align="left">• <b>Tiña de los pies.</b></p>'+
            '<p align="left">• <b>Distrofia ungueal preexistente (Por ej: En los pacientes con psoriasis).</b></p>'+
            '<p align="left">• <b>Edad avanzada.</b></p>'+
            '<p align="left">• <b>Sexo masculino.</b></p>'+
            '<p align="left">• <b>Contacto con alguien con tiña del pie u onicomicosis (Por ej: Un miembro de la familia o en un baño público).</b></p>'+
            '<p align="left">• <b>Enfermedad vascular periférica o diabetes.</b></p>'+
          '</div>',
          padding: '0',
          width: '70%',
          grow: 'fullscreen',
          showCloseButton: true,
          showCancelButton: false,
          focusConfirm: false,
          confirmButtonText: '<i class="fa fa-thumbs-up"></i> cerrar',
          confirmButtonAriaLabel: 'Thumbs up, great!',
        });
      }    


}

