import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'sb-navbar-inicio',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './navbar-inicio.component.html',
    styleUrls: ['navbar-inicio.component.scss'],
})
export class NavbarInicioComponent implements OnInit {
    constructor(private router: Router,) {}
    ngOnInit() {}

    login(){
        this.router.navigate(['/login']);
    }

    registro(){
        this.router.navigate(['/registro/general']);
    }

    toAgendarHora(){
        document.getElementById("disponibilidad-horaria").scrollIntoView({behavior:"smooth"});
    }

    toServicios(){
        document.getElementById("servicios").scrollIntoView({behavior:"smooth"});
    }

    toMisionVision(){
        document.getElementById("misionVision").scrollIntoView({behavior:"smooth"});
    }

    toAboutServices(){
        document.getElementById("about-services").scrollIntoView({behavior:"smooth"});
    }

    toContacto(){
        document.getElementById("contacto").scrollIntoView({behavior:"smooth"});
    }
}
