import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { InicioService } from '@modules/inicio/services';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'sb-editar-inicio',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './editar-inicio.component.html',
    styleUrls: ['editar-inicio.component.scss'],
})
export class EditarInicioComponent implements OnInit {
    inicio: any;

    constructor(private inicioService: InicioService, private modal: NgbModal,private router: Router) {
    }

    ngOnInit() {
        this.obtenerListado();
    }

    openxl(contenido) {
        this.modal.open(contenido,{size:'xl',centered:true});
    }

    editar(titulo, subTitulo, mision, vision, valorBasica, valorAvanzada, valorClinica, curacion, reconstruccion) {
        let data = {
            id:this.inicio.id,
            titulo,subTitulo, mision, vision, valorBasica, valorAvanzada, valorClinica, curacion, reconstruccion
        }
        console.log(data);
        this.inicioService.editarPaginaInicio(data).subscribe((resp:any)=>{
            console.log(resp);
            if(resp.code===200){
                console.log("entro");
                this.inicio = resp.data;
                console.log(this.inicio);
                this.modal.dismissAll();

            }
        })
    }

    obtenerListado(){
        this.inicioService.obtenerListadoInicio().subscribe((resp: any) => {
            console.log(resp);
            this.inicio = resp.Inicio;
        })

    }
}
