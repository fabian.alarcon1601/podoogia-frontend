/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

/* Modules */
import { AppCommonModule } from '@common/app-common.module';
import { NavigationModule } from '@modules/navigation/navigation.module';

/* Components */
import * as fichasClinicasComponents from './components';

/* Containers */
import * as fichasClinicasContainers from './containers';

/* Guards */
import * as fichasClinicasGuards from './guards';

/* Services */
import * as fichasClinicasServices from './services';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        FormsModule,
        AppCommonModule,
        NavigationModule,
    ],
    providers: [...fichasClinicasServices.services, ...fichasClinicasGuards.guards],
    declarations: [...fichasClinicasContainers.containers, ...fichasClinicasComponents.components],
    exports: [...fichasClinicasContainers.containers, ...fichasClinicasComponents.components],
})
export class FichasClinicasModule {}
