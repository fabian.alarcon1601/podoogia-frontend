import { TestBed } from '@angular/core/testing';

import { FichasClinicasGuard } from './fichas-clinicas.guard';

describe('FichasClinicas Guards', () => {
    let fichasClinicasGuard: FichasClinicasGuard;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [FichasClinicasGuard],
        });
        fichasClinicasGuard = TestBed.inject(FichasClinicasGuard);
    });

    describe('canActivate', () => {
        it('should return an Observable<boolean>', () => {
            fichasClinicasGuard.canActivate().subscribe(response => {
                expect(response).toEqual(true);
            });
        });
    });

});
