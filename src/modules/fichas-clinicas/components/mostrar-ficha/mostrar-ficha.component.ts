import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FichasClinicasService } from '@modules/fichas-clinicas/services';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'sb-mostrar-ficha',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './mostrar-ficha.component.html',
    styleUrls: ['mostrar-ficha.component.scss'],
})
export class MostrarFichaComponent implements OnInit {
    paciente_id;
    ficha_clinica;
    edad;
    historiales;
    antecedentes;
    antecedentesPodologicos;
    historialSesiones;
    diagnosticos;
    constructor(private router: Router, 
        private agregarFichaService: FichasClinicasService,private modal: NgbModal) {
        const navigation = this.router.getCurrentNavigation();
        this.paciente_id = navigation.extras.state.paciente_id;
        let data = {
            id: this.paciente_id,
        }
        this.agregarFichaService.obtenerFichaClinica(data).subscribe((resp: any) => {
            this.ficha_clinica = resp.data;
            this.antecedentes = resp.antecedentes;
            this.antecedentesPodologicos = resp.antecedentespodologicos;
            this.historialSesiones = resp.historialSesiones;
            this.diagnosticos = resp.diagnostico;

            console.log(this.antecedentesPodologicos);
            console.log(this.antecedentes);

        });
    }
    ngOnInit() {
        
    }

    
    openl(contenido) {
        this.modal.open(contenido,{size:'xl',centered:true});
    }

    atras() {
        this.router.navigate(['/fichas/clinicas/ficha/clinica']);
    }

    
    
}

