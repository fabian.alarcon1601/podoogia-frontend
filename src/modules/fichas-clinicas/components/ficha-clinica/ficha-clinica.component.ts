import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { FichasClinicasService } from '@modules/fichas-clinicas/services';

@Component({
    selector: 'sb-ficha-clinica',
    templateUrl: './ficha-clinica.component.html',
    styleUrls: ['ficha-clinica.component.scss'],
})
export class FichaClinicaComponent implements OnInit {
    pacientes;
    constructor(private fichaClinicaService: FichasClinicasService, private router:Router) {}
    ngOnInit() {
        this.fichaClinicaService.obtenerListadoPacientes().subscribe((resp:any)=>{
            console.log(resp.Pacientes);
            this.pacientes=resp.Pacientes;
        })
    }
    irACrearFicha(paciente_id){
        //aparte el estado un valor que me identifique que voy a crear
        console.log(paciente_id);
        const navigationExtras: NavigationExtras = {state: {paciente_id: paciente_id, editar: 0}};
        this.router.navigate(['/agregar/ficha/clinica'],navigationExtras);
    }
    irAVerFicha(paciente_id){
        console.log(paciente_id);
        const navigationExtras: NavigationExtras = {state: {paciente_id: paciente_id}};
        this.router.navigate(['fichas/clinicas/mostrar/ficha'],navigationExtras);
    }
    irAModificar(paciente_id){
        console.log(paciente_id);
        //aparte el estado un valor que me identifique que voy a modificar
        const navigationExtras: NavigationExtras = {state: {paciente_id: paciente_id, editar: 1}};
        this.router.navigate(['/agregar/ficha/clinica'],navigationExtras);
    }
    buscar(nombre){
        let data= {nombre};
        this.fichaClinicaService.buscarPacientePorNombre(data).subscribe((resp:any)=>{
            this.pacientes=resp.data;
        })
    }
    crearpaciente(){
        this.router.navigate(['/fichas/clinicas/crear/paciente'])
    }
}
