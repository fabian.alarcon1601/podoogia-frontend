import { FichaClinicaComponent } from './ficha-clinica/ficha-clinica.component';
import { MostrarFichaComponent } from '../../fichas-clinicas/components/mostrar-ficha/mostrar-ficha.component';
import { CrearUsuarioComponent } from './crear-usuario/crear-usuario.component';

export const components = [FichaClinicaComponent, MostrarFichaComponent, CrearUsuarioComponent];

export * from './ficha-clinica/ficha-clinica.component';
export * from '../../fichas-clinicas/components/mostrar-ficha/mostrar-ficha.component';
export * from './crear-usuario/crear-usuario.component';
