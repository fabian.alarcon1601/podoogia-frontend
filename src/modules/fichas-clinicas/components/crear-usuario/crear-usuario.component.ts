import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FichasClinicasService } from '@modules/fichas-clinicas/services';
import Swal from 'sweetalert2';

@Component({
    selector: 'sb-crear-usuario',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './crear-usuario.component.html',
    styleUrls: ['crear-usuario.component.scss'],
})
export class CrearUsuarioComponent implements OnInit {
    comunas: any;
    id;
    constructor(private router: Router, private fichaClinicaService: FichasClinicasService) { }
    ngOnInit() {
        this.fichaClinicaService.listarComunas().subscribe((resp: any) => {
            console.log(resp.Comuna);
            this.comunas = resp.Comuna;
        })
    }

    guardarPaciente(nombres, apellido_paterno, apellido_materno, correo, telefono, rut, n_emergencia, comuna_id) {
        let data;
        if (nombres == "" || apellido_paterno == "" || apellido_materno == "" || correo == "" || telefono == "" || rut == "" || comuna_id == "") {
            data = {
                nombres:null,
                apellido_paterno:null,
                apellido_materno:null,
                correo:null,
                telefono:null,
                rut:null,
                n_emergencia:null,
                comuna_id: null
            };
        } else {
            data = {

                nombres,
                apellido_paterno,
                apellido_materno,
                correo,
                telefono,
                rut,
                n_emergencia,
                comuna_id
            }
        } 
        this.fichaClinicaService.crearPacientes(data).subscribe((resp: any) => {
            if (resp.code === 200) {
                Swal.fire({
                    position: 'bottom-end',
                    icon: 'success',
                    title: 'Paciente creado correctamente',
                    showConfirmButton: false,
                    timer: 1500
                })
                this.router.navigate(['/fichas/clinicas/ficha/clinica'])
            } else {
                if (resp.code === 400) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se a podido crear el paciente',

                    })
                }
            }
        })
    }

    cancelarCreacion() {
        this.router.navigate(['fichas/clinicas/ficha/clinica']);
    }
}
