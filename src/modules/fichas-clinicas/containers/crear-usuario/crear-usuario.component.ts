import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
    selector: 'sb-crear-usuario-container',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './crear-usuario.component.html',
    styleUrls: ['crear-usuario.component.scss'],
})
export class CrearUsuarioComponent implements OnInit {
    constructor() {}
    ngOnInit() {}
}
