import { FichaClinicaComponent } from './ficha-clinica/ficha-clinica.component';
import { MostrarFichaComponent } from './mostrar-ficha/mostrar-ficha.component';
import { CrearUsuarioComponent } from './crear-usuario/crear-usuario.component';

export const containers = [FichaClinicaComponent, MostrarFichaComponent, CrearUsuarioComponent];

export * from './ficha-clinica/ficha-clinica.component';
export * from './mostrar-ficha/mostrar-ficha.component';
export * from './crear-usuario/crear-usuario.component';
 