import { TestBed } from '@angular/core/testing';

import { FichasClinicasService } from './fichas-clinicas.service';

describe('FichasClinicasService', () => {
    let fichasClinicasService: FichasClinicasService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [FichasClinicasService],
        });
        fichasClinicasService = TestBed.inject(FichasClinicasService);
    });

    describe('getFichasClinicas$', () => {
        it('should return Observable<FichasClinicas>', () => {
            expect(fichasClinicasService).toBeDefined();
        });
    });
});
