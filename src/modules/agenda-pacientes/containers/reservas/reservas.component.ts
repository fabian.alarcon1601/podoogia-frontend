import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
    selector: 'sb-reservas-container',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './reservas.component.html',
    styleUrls: ['reservas.component.scss'],
})
export class ReservasComponent implements OnInit {
    constructor() {}
    ngOnInit() {}
}
