import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
    selector: 'sb-agenda-profesional-container',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './agenda-profesional.component.html',
    styleUrls: ['agenda-profesional.component.scss'],
})
export class AgendaProfesionalComponent implements OnInit {
    constructor() {}
    ngOnInit() {}
}
