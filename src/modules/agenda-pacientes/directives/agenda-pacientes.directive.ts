import { Directive, Input } from '@angular/core';

@Directive({
    selector: '[sbAgendaPacientes]',
})
export class AgendaPacientesDirective {
    @Input() param!: string;

    constructor() {}
}
