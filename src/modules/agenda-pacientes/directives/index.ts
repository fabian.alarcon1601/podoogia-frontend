import { AgendaPacientesDirective } from './agenda-pacientes.directive';

export const directives = [AgendaPacientesDirective];

export * from './agenda-pacientes.directive';
