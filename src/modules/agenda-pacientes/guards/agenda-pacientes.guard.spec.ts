import { TestBed } from '@angular/core/testing';

import { AgendaPacientesGuard } from './agenda-pacientes.guard';

describe('AgendaPacientes Guards', () => {
    let agendaPacientesGuard: AgendaPacientesGuard;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [AgendaPacientesGuard],
        });
        agendaPacientesGuard = TestBed.inject(AgendaPacientesGuard);
    });

    describe('canActivate', () => {
        it('should return an Observable<boolean>', () => {
            agendaPacientesGuard.canActivate().subscribe(response => {
                expect(response).toEqual(true);
            });
        });
    });

});
