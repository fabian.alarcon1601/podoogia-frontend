import { AgendaPacientesGuard } from './agenda-pacientes.guard';

export const guards = [AgendaPacientesGuard];

export * from './agenda-pacientes.guard';
