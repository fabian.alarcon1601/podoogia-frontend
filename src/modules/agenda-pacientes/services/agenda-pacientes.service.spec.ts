import { TestBed } from '@angular/core/testing';

import { AgendaPacientesService } from './agenda-pacientes.service';

describe('AgendaPacientesService', () => {
    let agendaPacientesService: AgendaPacientesService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [AgendaPacientesService],
        });
        agendaPacientesService = TestBed.inject(AgendaPacientesService);
    });

    describe('getAgendaPacientes$', () => {
        it('should return Observable<AgendaPacientes>', () => {
            expect(agendaPacientesService).toBeDefined();
        });
    });
});
