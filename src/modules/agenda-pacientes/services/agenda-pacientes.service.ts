import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import {url} from '../../../environments/environment';

@Injectable()
export class AgendaPacientesService {
    private headers = new HttpHeaders(
        {  'Content-Type':  'application/json' }
    );
    constructor(private http:HttpClient) {}

    obtenerListadoReservas(){
        return this.http.get(url + '/reserva/listar');
    }



}
