/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SBRouteData } from '@modules/navigation/models';

/* Module */
import { AgendaPacientesModule } from './agenda-pacientes.module';

/* Containers */
import * as agendaPacientesContainers from './containers';

/* Guards */
import * as agendaPacientesGuards from './guards';

/* Routes */
export const ROUTES: Routes = [
    {
        path: 'agenda',
        data: {
            title: 'Agenda Profesional',
        } as SBRouteData,
        canActivate: [],
        component: agendaPacientesContainers.AgendaProfesionalComponent,
    },
    {
        path: 'reservas',
        data: {
            title: 'Horas reservadas',
        } as SBRouteData,
        canActivate: [],
        component: agendaPacientesContainers.ReservasComponent,
    }
];

@NgModule({
    imports: [AgendaPacientesModule, RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
})
export class AgendaPacientesRoutingModule {}
