/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

/* Modules */
import { AppCommonModule } from '@common/app-common.module';
import { NavigationModule } from '@modules/navigation/navigation.module';

/* Components */
import * as agendaPacientesComponents from './components';

/* Containers */
import * as agendaPacientesContainers from './containers';

/* Guards */
import * as agendaPacientesGuards from './guards';

/* Services */
import * as agendaPacientesServices from './services';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        FormsModule,
        AppCommonModule,
        NavigationModule,
    ],
    providers: [...agendaPacientesServices.services, ...agendaPacientesGuards.guards],
    declarations: [...agendaPacientesContainers.containers, ...agendaPacientesComponents.components],
    exports: [...agendaPacientesContainers.containers, ...agendaPacientesComponents.components],
})
export class AgendaPacientesModule {}
