import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AgendaPacientesService } from '@modules/agenda-pacientes/services';

@Component({
    selector: 'sb-agenda-profesional',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './agenda-profesional.component.html',
    styleUrls: ['agenda-profesional.component.scss'],
})
export class AgendaProfesionalComponent implements OnInit {
    reservas;
    constructor(private agendaService: AgendaPacientesService, private router: Router) { }
    ngOnInit() {
        this.agendaService.obtenerListadoReservas().subscribe((resp: any) => {
            console.log(resp.Reservas);
            this.reservas = resp.Reservas;
            console.log(this.reservas);
        })
    }

}
