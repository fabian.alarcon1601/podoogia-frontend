import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { AgendaPacientesService } from '@modules/agenda-pacientes/services';

@Component({
    selector: 'sb-reservas',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './reservas.component.html',
    styleUrls: ['reservas.component.scss'],
})
export class ReservasComponent implements OnInit {
    reservas;
    constructor(private agendaService: AgendaPacientesService) {}
    ngOnInit() {
        this.agendaService.obtenerListadoReservas().subscribe((resp: any) => {
            console.log(resp.Reservas);
            this.reservas = resp.Reservas;
            console.log(this.reservas);
        })
    }   
}
