import { AgendaProfesionalComponent } from './agenda-profesional/agenda-profesional.component';
import { ReservasComponent } from './reservas/reservas.component';

export const components = [AgendaProfesionalComponent, ReservasComponent];

export * from './agenda-profesional/agenda-profesional.component';
export * from './reservas/reservas.component';
