import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
    selector: 'sb-perfil-container',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './perfil.component.html',
    styleUrls: ['perfil.component.scss'],
})
export class PerfilComponent implements OnInit {
    constructor() {}
    ngOnInit() {}
}
