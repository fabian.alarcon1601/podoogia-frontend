import { ContenidoLoginComponent } from './contenido-login/contenido-login.component';
import { PerfilComponent } from './perfil/perfil.component';

export const containers = [ContenidoLoginComponent, PerfilComponent];

export * from './contenido-login/contenido-login.component';
export * from './perfil/perfil.component';
