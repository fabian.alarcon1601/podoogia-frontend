import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
    selector: 'sb-contenido-login-container',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './contenido-login.component.html',
    styleUrls: ['contenido-login.component.scss'],
})
export class ContenidoLoginComponent implements OnInit {
    constructor() {}
    ngOnInit() {}
}
