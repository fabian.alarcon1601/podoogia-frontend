import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { url } from "../../../environments/environment";

@Injectable()
export class LoginService {
    private headers = new HttpHeaders(
        {  'Content-Type':  'application/json' }
      );
    constructor(private http: HttpClient) {}

    login(data){
        const option = {headers:this.headers};
        return this.http.post(url + '/usuario/login',data, option);
    }
    logout(data){
        const option = {headers:this.headers};
        return this.http.post(url + '/usuario/logout',data, option);
       
    }

}
