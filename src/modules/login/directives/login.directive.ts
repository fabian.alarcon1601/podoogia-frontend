import { Directive, Input } from '@angular/core';

@Directive({
    selector: '[sbLogin]',
})
export class LoginDirective {
    @Input() param!: string;

    constructor() {}
}
