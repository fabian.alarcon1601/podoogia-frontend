import { LoginGuard } from './login.guard';

export const guards = [LoginGuard];

export * from './login.guard';
