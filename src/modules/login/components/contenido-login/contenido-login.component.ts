import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '@modules/login/services';
import Swal from 'sweetalert2';

@Component({
    selector: 'sb-contenido-login',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './contenido-login.component.html',
    styleUrls: ['contenido-login.component.scss'],
})
export class ContenidoLoginComponent implements OnInit {
    showPass;
    showEmail;
    showMessage;
    message;
    constructor(private loginService: LoginService, private router: Router) {

    }
    ngOnInit() {
        this.showEmail = false;
        this.showPass = false;
        this.showMessage = false;
    }

    login(correo, pass) {

        console.log(correo, pass);
        if (this.validar(correo, pass)) {
            let data = { correo, pass };
            this.loginService.login(data).subscribe((resp: any) => {
                console.log(resp);
                if (resp.code === 200) {
                    localStorage.setItem('usuario', JSON.stringify((resp.data)));
                    if (resp.data.rol.nombre == 'profesional') {
                        this.router.navigate(['/fichas/clinicas/ficha/clinica']).then(() => { window.location.reload() });

                    } else {
                        if (resp.data.rol.nombre == 'admin') {
                            this.router.navigate(['/solicitudes/psicologos/listado/solicitudes']).then(() => { window.location.reload() });
                        }
                    }
                } else {
                    this.showMessage = true;

                    Swal.fire({
                        title:
                            this.message=resp.message,
                        html:
                            '',
                        icon: 'error',
                        showConfirmButton: false,
                        timer: 1500
                    });

                }
            })
        }
    }
    validar(correo, pass) {
        if (!correo && !pass) {
            this.showEmail = true;
            this.showPass = true;
            return false;
        } else {
            if (!correo) {
                this.showEmail = true;
                return false;
            } else {
                this.showEmail = false;
                if (!pass) {
                    this.showPass = true;
                    return false;
                } else {
                    this.showPass = false;
                    return true;
                }

            }
        }
    }
    inicio() {
        this.router.navigate(['/inicio']);
    }
    registro() {
        this.router.navigate(['/registro/general']);
    }
}
