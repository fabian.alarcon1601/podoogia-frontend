/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/* Module */
import { LoginModule } from './login.module';

/* Containers */
import * as loginContainers from './containers';

/* Guards */
import * as loginGuards from './guards';
import { SBRouteData } from '@modules/navigation/models';

/* Routes */
export const ROUTES: Routes = [
    {
        path: '',
        data: {
            title: 'login',
        } as SBRouteData,
        canActivate: [],
        component: loginContainers.ContenidoLoginComponent,
    },
    {
        path: 'perfil',
        data: {
            title: 'Edición',
        } as SBRouteData,
        canActivate: [],
        component: loginContainers.PerfilComponent
    },
];
@NgModule({
    imports: [LoginModule, RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
})
export class LoginRoutingModule {}
