import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '@modules/auth/services';
import { LoginService } from '@modules/login/services';
import { SideNavService } from '@modules/navigation/services';

@Component({
    selector: 'sb-top-nav-user',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './top-nav-user.component.html',
    styleUrls: ['top-nav-user.component.scss'],
})
export class TopNavUserComponent implements OnInit {
    user;
    constructor(private sideNavServices:SideNavService, private router:Router, public userService: UserService) {}
    ngOnInit() {
        this.user= JSON.parse(localStorage.getItem('usuario')) ;
        console.log(this.user);
    }

    logout(){
        let localUser = JSON.parse(localStorage.getItem('usuario')) ;
        let data = { id: localUser.id};
        this.sideNavServices.logout(data).subscribe((resp:any)=>{
            if(resp.code===200){ 
                this.router.navigate(['/inicio']);
                localStorage.clear();
            }
        })
    }
}
