import { isNgTemplate } from '@angular/compiler';
import { SideNavItems, SideNavSection } from '@modules/navigation/models';

let localUser = JSON.parse(localStorage.getItem('usuario'));

export const sideNavSections: SideNavSection[] = items(localUser);

export const sideNavItems: SideNavItems = {
    fichaClinica: {
        icon: 'clipboard-list',
        text: 'Fichas pacientes',
        link: '/fichas/clinicas/ficha/clinica',
    },
    solicitudes: {
        icon: 'list-alt',
        text: 'Solicitudes pendientes',
        link: '/solicitudes/psicologos/listado/solicitudes',
    },
    solicitudesRechazadas: {
        icon: 'list-alt',
        text: 'Solicitudes rechazadas',
        link: '/solicitudes/psicologos/solicitudes/rechazadas',
    },
    solicitudesAprobadas: {
        icon: 'list-alt',
        text: 'Solicitudes aprobadas',
        link: '/solicitudes/psicologos/solicitudes/aprobadas',
    },
    horario: {
        icon: 'list-alt',
        text: 'Cargar horario ',
        link: '/horario/carga/horario',
    },
    inicio:{
        icon: 'list-alt',
        text: 'Inicio',
        link: '/inicio/editar/inicio',
    },
    footer:{
        icon: 'list-alt',
        text: 'Footer',
        link: '/inicio/editar/footer',
    },

    listadoHorasDia :{
        icon: 'list-alt',
        text: 'Horas reservadas',
        link: '/paciente/reservas',
    },
    agendarHora:{
        icon: 'list-alt',
        text: 'Agendar hora',
        link: '/agendar/hora/agendar',
    },

    agenda:{
        icon: 'list-alt',
        text: 'Agenda del dia',
        link: '/paciente/agenda',
    }



};

function items(localUser) {
    console.log(localUser);
    let arrayItems = [];
    console.log(arrayItems);

    try {
       
        if (localUser.rol.nombre  == 'admin') {
            arrayItems = [
                {
                    text: 'Solicitudes Podologos',
                    items: ['solicitudes', 'solicitudesAprobadas', 'solicitudesRechazadas' ],
                },
                {
                    text: 'Gestion de inicio',
                    items: ['inicio','footer'],
                },
            ]
        } else {
            if (localUser.rol.nombre  == 'profesional') {
                arrayItems = [
                    {
                        text: 'Paciente',
                        items: ['fichaClinica', 'agendarHora' ],
                    },
                    {
                        text: 'Podologo',
                        items: ['horario', 'agenda', 'listadoHorasDia'],
                    },
                ]
            }
        }
        return arrayItems;

    } catch (error) {
        console.error();
    }

}