import { TestBed } from '@angular/core/testing';

import { HorarioGuard } from './horario.guard';

describe('Horario Guards', () => {
    let horarioGuard: HorarioGuard;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [HorarioGuard],
        });
        horarioGuard = TestBed.inject(HorarioGuard);
    });

    describe('canActivate', () => {
        it('should return an Observable<boolean>', () => {
            horarioGuard.canActivate().subscribe(response => {
                expect(response).toEqual(true);
            });
        });
    });

});
