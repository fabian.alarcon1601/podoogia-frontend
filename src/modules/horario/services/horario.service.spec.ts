import { TestBed } from '@angular/core/testing';

import { HorarioService } from './horario.service';

describe('HorarioService', () => {
    let horarioService: HorarioService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [HorarioService],
        });
        horarioService = TestBed.inject(HorarioService);
    });

    describe('getHorario$', () => {
        it('should return Observable<Horario>', () => {
            expect(horarioService).toBeDefined();
        });
    });
});
