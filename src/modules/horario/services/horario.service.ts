import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { url } from 'environments/environment';
import { Observable, of } from 'rxjs';

@Injectable()
export class HorarioService {
    private headers = new HttpHeaders(
        {  'Content-Type':  'application/json' }
      );
    constructor(private http: HttpClient) {}


    cargarHorario(data){
        const option = {headers:this.headers};
        return this.http.post(url + '/horario/crear',data, option);
    }
    buscarDisponibilidad(data){
        const option = {headers:this.headers};
        return this.http.post(url + '/horario/buscar/disponibilidad',data, option);
    }
    obtenerPodologoPorUsuarioID(data){
        const option = {headers:this.headers};
        console.log(data);
        return this.http.post(url + '/profesional/buscar/profesional/usuario', data, option);
    }

}
