import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
    selector: 'sb-carga-horario-container',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './carga-horario.component.html',
    styleUrls: ['carga-horario.component.scss'],
})
export class CargaHorarioComponent implements OnInit {
    constructor() {}
    ngOnInit() {}
}
