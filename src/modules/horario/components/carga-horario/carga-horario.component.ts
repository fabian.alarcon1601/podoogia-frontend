import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { HorarioService } from '@modules/horario/services';
import Swal from 'sweetalert2';

@Component({
    selector: 'sb-carga-horario',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './carga-horario.component.html',
    styleUrls: ['carga-horario.component.scss'],
})
export class CargaHorarioComponent implements OnInit {
    showSucces;
    showError;
    message;
    messageInicioMes;
    messageFinalMes;
    messageInicioDia;
    messageFinalDia;
    messageInicioHora;
    messageFinalHora;
    constructor(private horarioService: HorarioService) {


    }
    ngOnInit() {
        this.showSucces = false;
        this.showError = false;
        this.messageInicioMes = false;
        this.messageFinalMes = false;
        this.messageInicioDia = false;
        this.messageFinalDia = false;
        this.messageInicioHora = false;
        this.messageFinalHora = false;
    }

    cargar(mes_inicial:Number, mes_final:Number, dia_inicial:Number, dia_final:Number, hora_inicial:Number, hora_final:Number) {
        let localUser: any = JSON.parse(localStorage.getItem('usuario'));
        let userData = { id: localUser.id };
        this.horarioService.obtenerPodologoPorUsuarioID(userData).subscribe((resp: any) => {
            let profesional_id = resp.data.id;
            let data;

            if (this.validacionCampos(mes_inicial, mes_final, dia_inicial, dia_final, hora_inicial, hora_final)) {
                data = {
                    mesInicio: mes_inicial,
                    mesFinal: mes_final,
                    diaInicio: dia_inicial,
                    diaFinal: dia_final,
                    horaInicio: hora_inicial,
                    horaFinal: hora_final,
                    profesional_id
                };

                this.horarioService.cargarHorario(data).subscribe((resp: any) => {
                    console.log(resp);
                    if (resp.code === 200) {
                        Swal.fire({
                            position: 'bottom-end',
                            icon: 'success',
                            title: this.message = resp.message,
                            showConfirmButton: false,
                            timer: 1500
                        })
                    } else {
                        if (resp.code === 400) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: this.message = resp.message,

                            })
                        }

                    }
                })

            }else{
                data = {
                   
                };
                this.horarioService.cargarHorario(data).subscribe((resp: any) => {
                    if (resp.code === 400) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: this.message = resp.message,

                        })
                    }
                })
            }


        })


    }

    validacionCampos(mes_inicial, mes_final, dia_inicial, dia_final, hora_inicial, hora_final) {
        if (!mes_inicial && !mes_final && !dia_inicial && !dia_final && !hora_inicial && !hora_final) {
            this.messageInicioMes = true;
            this.messageFinalMes = true;
            this.messageInicioDia = true;
            this.messageFinalDia = true;
            this.messageInicioHora = true;
            this.messageFinalHora = true;
            return false;
        } else {
            if (!mes_final && !dia_inicial && !dia_final && !hora_inicial && !hora_final) {
                    this.messageFinalMes = true;
                    this.messageInicioDia = true;
                    this.messageFinalDia = true;
                    this.messageInicioHora = true;
                    this.messageFinalHora = true;
                    return false;
                }else{
                    if (!dia_inicial && !dia_final && !hora_inicial && !hora_final) {
                        this.messageInicioDia = true;
                        this.messageFinalDia = true;
                        this.messageInicioHora = true;
                        this.messageFinalHora = true;
                        return false;
                    }else{
                        if (!dia_final && !hora_inicial && !hora_final) {
                            this.messageFinalDia = true;
                            this.messageInicioHora = true;
                            this.messageFinalHora = true;
                            return false;
                        }
                    }
                }
            } 
            
        
        return true;

    }

    
}
