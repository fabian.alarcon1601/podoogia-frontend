/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

/* Modules */
import { AppCommonModule } from '@common/app-common.module';
import { NavigationModule } from '@modules/navigation/navigation.module';

/* Components */
import * as horarioComponents from './components';

/* Containers */
import * as horarioContainers from './containers';

/* Guards */
import * as horarioGuards from './guards';

/* Services */
import * as horarioServices from './services';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        FormsModule,
        AppCommonModule,
        NavigationModule,
    ],
    providers: [...horarioServices.services, ...horarioGuards.guards],
    declarations: [...horarioContainers.containers, ...horarioComponents.components],
    exports: [...horarioContainers.containers, ...horarioComponents.components],
})
export class HorarioModule {}
