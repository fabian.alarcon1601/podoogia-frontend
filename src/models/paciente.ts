export class PacienteModel{
    id:number;
    nombres:string;
    apellido_paterno: string;
    apellido_materno: string;
    correo: string;
    rut: string;
    telefono: string;    
    comuna_id:number;
    constructor(){ }
}